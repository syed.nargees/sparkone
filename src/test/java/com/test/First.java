package com.test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

public class First {
    public static void main(String[] args) {
        //System.setProperty("webdriver.gecko.driver", "path-to-your-geckodriver");

        WebDriver driver = new FirefoxDriver();

        // Navigate to the application home page
        driver.get("https://demowebshop.tricentis.com/");

        // Click on the login link
        WebElement loginLink = driver.findElement(By.linkText("Log in"));
        loginLink.click();

        // get the Email textbox
        WebElement email = driver.findElement(By.id("Email"));
        email.clear();

        // enter Email
        email.sendKeys("tester05@gmail.com");

        // get the Password textbox
        WebElement password = driver.findElement(By.id("Password"));
        password.clear();

        // enter Password
        password.sendKeys("test@123");

        // submit the form
        WebElement loginButton = driver.findElement(By.xpath("//input[@value='Log in']"));
        loginButton.click();
        System.out.println("Nargees");
    }   
}
   
